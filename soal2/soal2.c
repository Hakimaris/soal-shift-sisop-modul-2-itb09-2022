#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>

int extractzip(char *filename, char *targetdir) 
{
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) 
  {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0) 
  {
    char *argv[] = {"mkdir", "-p", targetdir, NULL};
    execv("/bin/mkdir", argv);
  } 
  else 
  {
    while ((wait(&status)) > 0);
    {
      char *argv[] = {"unzip", filename, "*.png", "-d", targetdir, NULL};
      execv("/usr/bin/unzip", argv);
    }
    
  }
}

int main()
{
    chdir("/home/naufal/shift2/");
    
    char filename[] = "drakor.zip";
    char targetdir[] = "drakor";

    extractzip(filename, targetdir);
}