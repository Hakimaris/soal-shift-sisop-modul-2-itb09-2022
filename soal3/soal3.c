#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdbool.h>

int main()
{

    pid_t
        child_darat_id,
        child_air_id,
        child_zip_id,
        move_darat,
        move_air,
        move_burung,
        move_ikan,
        delete_kodok,
        delete_burung_1,
        delete_burung_2;

    int stats;

    child_darat_id = fork();

    if (child_darat_id < 0)
        exit(EXIT_FAILURE); // Apabila gagal maka program berhenti

    if (child_darat_id == 0)
    {
        char *argv[] = {"mkdir", "-p", "/home/zygantic/modul2/darat", NULL};
        execv("/bin/mkdir", argv);
    }
    else
    {
        // parent
        while ((wait(&stats)) > 0)
            ;

        child_air_id = fork();

        if (child_air_id < 0)
            exit(EXIT_FAILURE); // Apabila gagal maka program berhenti

        if (child_air_id == 0)
        {
            sleep(3);
            char *argv[] = {"mkdir", "-p", "/home/zygantic/modul2/air", NULL};
            execv("/bin/mkdir", argv);
        }
        else
        {
            while ((wait(&stats)) > 0)
                ;

            child_zip_id = fork();

            if (child_zip_id < 0)
                exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti

            if (child_zip_id == 0)
            {
                char *argv[] = {"unzip", "/home/zygantic/modul2/animal.zip", NULL};
                execv("/usr/bin/unzip", argv);
            }
            else
            {
                while ((wait(&stats)) > 0)
                    ;
                move_darat = fork(); // Mempindahkan gambar darat

                if (move_darat < 0)
                {
                    exit(EXIT_FAILURE);
                }

                if (move_darat == 0)
                {
                    execl("/usr/bin/find", "find", "/home/zygantic/modul2/animal", "-type", "f", "-name", "*darat.jpg", "-exec", "mv", "-t", "/home/zygantic/modul2/darat", "{}", "+", (char *)NULL);
                }
                else
                {
                    while ((wait(&stats)) > 0)
                        ;
                    move_air = fork(); // Mempindahkan gambar air

                    if (move_air < 0)
                    {
                        exit(EXIT_FAILURE);
                    }

                    if (move_air == 0)
                    {
                        execl("/usr/bin/find", "find", "/home/zygantic/modul2/animal", "-type", "f", "-name", "*air.jpg", "-exec", "mv", "-t", "/home/zygantic/modul2/air", "{}", "+", (char *)NULL);
                    }
                    else
                    {
                        while ((wait(&stats)) > 0)
                            ;
                        move_burung = fork(); // Mempindahkan gambar burung

                        if (move_burung < 0)
                        {
                            exit(EXIT_FAILURE);
                        }

                        if (move_burung == 0)
                        {
                            execl("/usr/bin/find", "find", "/home/zygantic/modul2/animal", "-type", "f", "-name", "*bird.jpg", "-exec", "mv", "-t", "/home/zygantic/modul2/darat", "{}", "+", (char *)NULL);
                        }
                        else
                        {
                            while ((wait(&stats)) > 0)
                                ;
                            move_ikan = fork(); // Mempindahkan gambar ikan

                            if (move_ikan < 0)
                            {
                                exit(EXIT_FAILURE);
                            }

                            if (move_ikan == 0)
                            {
                                execl("/usr/bin/find", "find", "/home/zygantic/modul2/animal", "-type", "f", "-name", "*fish.jpg", "-exec", "mv", "-t", "/home/zygantic/modul2/air", "{}", "+", (char *)NULL);
                            }
                            else
                            {
                                while ((wait(&stats)) > 0)
                                    ;
                                delete_kodok = fork(); // Menghapus gambar kodok

                                if (delete_kodok < 0)
                                {
                                    exit(EXIT_FAILURE);
                                }

                                if (delete_kodok == 0)
                                {
                                    execl("/usr/bin/find", "find", "/home/zygantic/modul2/animal", "-type", "f", "-name", "*frog.jpg", "-exec", "rm", "-r", "{}", "+", (char *)NULL);
                                }
                                else
                                {
                                    while ((wait(&stats)) > 0)
                                        ;
                                    delete_burung_1 = fork(); // Menghapus gambar burung

                                    if (delete_burung_1 < 0)
                                    {
                                        exit(EXIT_FAILURE);
                                    }

                                    if (delete_burung_1 == 0)
                                    {
                                        execl("/usr/bin/find", "find", "/home/zygantic/modul2/darat", "-type", "f", "-name", "*bird_darat.jpg", "-exec", "rm", "-r", "{}", "+", (char *)NULL);
                                    }
                                    else
                                    {
                                        while ((wait(&stats)) > 0)
                                            ;
                                        delete_burung_2 = fork(); // Menghapus gambar burung

                                        if (delete_burung_2 < 0)
                                        {
                                            exit(EXIT_FAILURE);
                                        }

                                        if (delete_burung_2 == 0)
                                        {
                                            execl("/usr/bin/find", "find", "/home/zygantic/modul2/darat", "-type", "f", "-name", "*bird.jpg", "-exec", "rm", "-r", "{}", "+", (char *)NULL);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}