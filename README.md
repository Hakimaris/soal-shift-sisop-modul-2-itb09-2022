# soal-shift-sisop-modul-2-ITB09-2022

Hasil Pengerjaan Soal Shift Praktikum SISTEM OPERASI 2022 kelompok ITB09<br>
Anggota Kelompok:<br>

| Nama                           | NRP          | 
| -------------------------------| -------------| 
| Fatih Rian Hibatul Hakim       | `5027201066` | 
| Naufal Dhiya Ulhaq             | `5027201029` | 
| Kevin Oktoaria                 | `50272010xx` |


---
# Soal 1

## Soal 1a
### Analisa Soal

Pada soal ini praktikan diminta untuk mendownload serta mengekstrak dua buah file dari link yang sudah disediakan pada soal shift modul 2. setelah itu diminta untuk membuat folder bernama "GACHA_GACHA". Semua hal barusan harus dilakukan dengan beberapa batasan berikut:
<ol>
  <li>Tidak Boleh Memakai System()</li>
  <li>Tidak Boleh memakai command ``mkdir`` dan ``rename``</li>
  <li>Gunakanlah ''Fork'' dan ''Exec''</li>
  <li>Direktori ''.'' dan ''..'' tidak termasuk</li>
</ol>

### Cara pengerjaan

untuk pengerjaanny, kami membuat berbagai fungsi untuk memudahkan penjelasann code nomor 1a.

```c
void buatfolder(char *filename) //untuk membuat folder
{
  pid_t piaidi1 = fork();

  if (piaidi1 == 0)
  {
    char *foldah[] = {"mkdir", "-p", filename, NULL};
    execv("/bin/mkdir", foldah);
  }
}

```
Pada fungsi ini, kami membuat ``fork`` untuk membantu proses membuatan folder. untuk pembuatan folder sendiri, kami mengandalkan fungsi yang namanya ``execv`` dikarenakan batasan untuk menggunakan command ``system()`` dan sebagainya.

Adapun untuk proses download kedua file yang dibutuhkan. dilakukan dengan cara berikut:

```c
void donlodpool()
{
  pid_t piaidi1 = fork(), piaidi2 = fork();
  int status;
  // printf("%d, %d", piaidi1, piaidi2);

  char *weap[] = {"wget", "--quiet", "--no-check-certificate", "https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", "scambanner.zip", NULL};
  char *chara[] = {"wget", "--quiet", "--no-check-certificate", "https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", "pitygoblog.zip", NULL};

  // execv("/bin/wget",weap);
  // execv("/bin/wget",chara);

  if (piaidi1 == 0 && piaidi2 > 0)
  {
    execv("/bin/wget", chara);
  }
  else if (piaidi1 > 0 && piaidi2 == 0)
  {
    execv("/bin/wget", weap);
  }
  // else if (piaidi1 > 0 && piaidi2 > 0)
  // {
  //   sleep(5);
  //   char *argv1[] = {"unzip", "*.zip"};
  //   execv("/bin/unzip", argv1);
  // }
}
```
Caranya cukup sederhana. apa yang daya lakukan pertama-tama adalah menyiapkan dua ``char`` yang berisikan panggilan untuk mendownload file yang dibutuhkan serta menyiapkan fork untuk dipakai. setelah itu saya membuat if condition : apabila PiD1 == 0, maka download file characters. apabila PiD2 == 0 maka akan mendownload file weapon. semua itu akan terjadi dan apabila sudah terdownload maka akan langsung bisa dipakai dalam bentuk zip.

Setelah sudah dapat file yang dibutuhkan. maka akan terjalanlah fungsi untuk mengekstrak file tersebut:

```c
void lepas(){
    pid_t pida = fork();
    int status;
    sleep(30); //untuk pemberian waktu download karena koneksi dikos kurang bagus

    char *foldah[] = {"unzip", "*.zip"};

    execv("/bin/unzip",foldah); // harus make fork

```
### Kendala
<ol>
  <li>Belum Bisa Mengerjakan Nomor 1b,1c,1d,1e</li>
  <li>proses download dan ekstrak terkadang gagal tergantung dari kecepatan koneksi</li>
</ol>
---
SOAL 2
Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun untuk menyelesaikan pekerjaannya.
Soal 2a
Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift2/drakor”. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.
Pembahasan 2a
Untuk mengextract zip yang sudah diberikan, kami membuat sebuah fungsi dengan nama extractzip sebagai berikut:
int extractzip(char *filename, char *targetdir) {
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0) {
    char *argv[] = {"mkdir", "-p", targetdir, NULL};
    execv("/bin/mkdir", argv);
  } 
  else {
    while ((wait(&status)) > 0);
    {
      char *argv[] = {"unzip", filename, "*.png", "-d", targetdir, NULL};
      execv("/usr/bin/unzip", argv);
    }
    
  }
}
---
# Soal 2
Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun untuk menyelesaikan pekerjaannya.

## soal 2a
### Penyelesaian Soal
Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift2/drakor”. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.
Pembahasan 2a
Untuk mengextract zip yang sudah diberikan, kami membuat sebuah fungsi dengan nama extractzip sebagai berikut:
```c
int extractzip(char *filename, char *targetdir) {
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0) {
    char *argv[] = {"mkdir", "-p", targetdir, NULL};
    execv("/bin/mkdir", argv);
  } 
  else {
    while ((wait(&status)) > 0);
    {
      char *argv[] = {"unzip", filename, "*.png", "-d", targetdir, NULL};
      execv("/usr/bin/unzip", argv);
    }
    
  }
}
```
Funsgi tersebut akan menerima dua argument berupa pointer char terhadap string yangsudah kita siapkan sebelumnya. String pertama merupakan nama file zip yang akan di extract. Sedangkan string ke dua adalah nama directory tempat kita akan mengextract file zip tersebut.

Selanjutnya fungsi tersebut akan melakukan fork dimana child process akan melakukan pembuatan directory baru dengan menggunakan funsgi execv seperti berikut:
```c
char *argv[] = {"mkdir", "-p", targetdir, NULL};
execv("/bin/mkdir", argv);
```

Setalah berhasil membuat direktori baru, maka parent process akan melakukan extract sebagai berikut:
```c
char *argv[] = {"unzip", filename, "*.png", "-d", targetdir, NULL};
execv("/usr/bin/unzip", argv);
```

Pada penggunaan fungsi unzip, kami menspesifikkan hanya file-file dengan nama diakhiri .png yang akan diextract, sehingga folder-folder tidak penting akan tidak ikut terextract. Kami juga menspesifikkan direktori extract pada direktori yang sudah disiapkan di awal tadi.

---
# Soal 3

## Soal 3a

### Analisa soal

Pada soal ini, praktikan diminta untuk membuat 2 directory yaitu  ` /home/[USER]/modul2/darat ` dan ` /home/[USER]/modul2/air ` . Pertama-tama praktikan perlu membuat directory ` /home/[USER]/modul2/darat ` lalu setelah 3 detik kemudian directory ` /home/[USER]/modul2/air `.

### Cara Pengerjaan

Pertama kami menggunakan `fork()` ` pid_t `, lalu kita mendeklarasikan ` child_darat_id ` dan ` child_air_id ` untuk ` fork() `. Lalu kami menggunakan ` argv[] ` dan ` mkdir ` ` /home/zygantic/modul/darat ` untuk membuat directory darat, setelah itu kami ` sleep(3) ` untuk process menunggu selama 3 detik dan setelah 3 detik program akan menggunakan ` argv[] ` dan ` mkdir ` ` /home/zygantic/modul/air ` untuk membuat directory air.

```c++
int main()
{

    pid_t
        child_darat_id,
        child_air_id,
        child_zip_id,
        move_darat,
        move_air,
        move_burung,
        move_ikan,
        delete_kodok,
        delete_burung_1,
        delete_burung_2;

    int stats;

   child_darat_id = fork();

    if (child_darat_id < 0)
        exit(EXIT_FAILURE); // Apabila gagal maka program berhenti

    if (child_darat_id == 0)
    {
        char *argv[] = {"mkdir", "-p", "/home/zygantic/modul2/darat", NULL};
        execv("/bin/mkdir", argv);
    }
    else
    {
        // parent
        while ((wait(&stats)) > 0)
            ;

        child_air_id = fork();

        if (child_air_id < 0)
            exit(EXIT_FAILURE); // Apabila gagal maka program berhenti

        if (child_air_id == 0)
        {
            sleep(3);
            char *argv[] = {"mkdir", "-p", "/home/zygantic/modul2/air", NULL};
            execv("/bin/mkdir", argv);
        }
        else
```
Output:

![](./img/Output3a.png)

## Soal 3b

### Analisa Soal

Pada soal ini, praktikan diminta untuk membuat me-unzip atau me-extract file ` animal.zip ` yang telah diberi oleh kakak-kakak asisten dan kita perlu me-unzip file tersebut di ` /home/[USER]/modul2/darat `.

### Cara Pengerjaan

Untuk menyelesaikan soal ini, pertama kami mendeklarasikan ` child_zip_id ` untuk ` fork() `. Lalu disini kami menggunakan ` argv[] ` dan ` unzip ` ` /home/zygantic/modul2/animal.zip ` untuk me-unzip file ` animal.zip `, setelah itu kamu juga menggunakan ` execv ` untuk menjalankan perintah untuk me-unzip.

```c++
while ((wait(&stats)) > 0)
                ;

            child_zip_id = fork();

            if (child_zip_id < 0)
                exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti

            if (child_zip_id == 0)
            {
                char *argv[] = {"unzip", "/home/zygantic/modul2/animal.zip", NULL};
                execv("/usr/bin/unzip", argv);
            }
            else
```

Output:

![](./img/Output3b.png)

## Soal 3c

### Analisa Soal

Setelah file berhasil diunzip, praktikan diminta untuk mempisah gambar hewan sesuai kategori mereka masing-masing. Untuk hewan kategori darat dipindahkan ke directory ` /home/[USER]/modul2/darat ` dan untuk hewan kategori air dipindahkan ke directory ` /home/[USER]/modul2/air `. Lalu terdapat delay waktu selama 3 detik dalam pembuatan folder darat dan air. Setelah berhasil mempisahkan gambar-gambar hewan sesuai kategori mereka, praktikan diminta untuk menghapus gambar hewan yang tidak memiliki kategori dimana pada soal ini hanya terdapat 1 hewan yang tidak memiliki kategori yaitu kodok.

### Cara Pengerjaan

Untuk soal 3c, pertama kami mendeklarasikan 5 variable yaitu ` move_darat, move_air, move_burung, move_ikan, dan delete_kodok ` setelah mendeklarasikan 5 variable tersebut kami memulai dari yang darat dimana kami menggunakan ` execl `, ` find `, ` mv `, dan ` -t ` `/home/zygantic/modul2/animal ` untuk memindahkan gambar yang memiliki kalimat ` darat.jpg ` ke directory ` /home/zygantic/modul2/darat `. Selanjutnya kami memindahkan hewan yang berkategori air dimana menggunakan cara yang tidak beda jauh dari kategori darat hanya saja kita hanya perlu mencari gambar yang memiliki kalimat ` air.jpg ` lalu memindahkannya ke directory ` /home/zygantic/modul2/air `. Setelah itu kita juga perlu memindahkan gambar yang memiliki kalimat ` bird.jpg ` dan ` fish.jpg ` kedalam kategori mereka masing-masing yaitu untuk burung kedalam ` /home/zygantic/modul2/darat ` dan ikan kedalam ` /home/zygantic/modul2/air `. Terakhir kita perlu menghapus gambar kodok karena tidak termasuk dalam kedua kategori yang tersedia, disini kami menggunakan ` execl ` juga tetapi yang berbeda hanya di bagian ` rm ` yang berarti remove.

```c++
		while ((wait(&stats)) > 0)
                    ;
                move_darat = fork(); // Mempindahkan gambar darat

                if (move_darat < 0)
                {
                    exit(EXIT_FAILURE);
                }

                if (move_darat == 0)
                {
                    execl("/usr/bin/find", "find", "/home/zygantic/modul2/animal", "-type", "f", "-name", "*darat.jpg", "-exec", "mv", "-t", "/home/zygantic/modul2/darat", "{}", "+", (char *)NULL);
                }
                else
                {
                    while ((wait(&stats)) > 0)
                        ;
                    move_air = fork(); // Mempindahkan gambar air

                    if (move_air < 0)
                    {
                        exit(EXIT_FAILURE);
                    }

                    if (move_air == 0)
                    {
                        execl("/usr/bin/find", "find", "/home/zygantic/modul2/animal", "-type", "f", "-name", "*air.jpg", "-exec", "mv", "-t", "/home/zygantic/modul2/air", "{}", "+", (char *)NULL);
                    }
                    else
                    {
                        while ((wait(&stats)) > 0)
                            ;
                        move_burung = fork(); // Mempindahkan gambar burung

                        if (move_burung < 0)
                        {
                            exit(EXIT_FAILURE);
                        }

                        if (move_burung == 0)
                        {
                            execl("/usr/bin/find", "find", "/home/zygantic/modul2/animal", "-type", "f", "-name", "*bird.jpg", "-exec", "mv", "-t", "/home/zygantic/modul2/darat", "{}", "+", (char *)NULL);
                        }
                        else
                        {
                            while ((wait(&stats)) > 0)
                                ;
                            move_ikan = fork(); // Mempindahkan gambar ikan

                            if (move_ikan < 0)
                            {
                                exit(EXIT_FAILURE);
                            }

                            if (move_ikan == 0)
                            {
                                execl("/usr/bin/find", "find", "/home/zygantic/modul2/animal", "-type", "f", "-name", "*fish.jpg", "-exec", "mv", "-t", "/home/zygantic/modul2/air", "{}", "+", (char *)NULL);
                            }
                            else
                            {
                                while ((wait(&stats)) > 0)
                                    ;
                                delete_kodok = fork(); // Menghapus gambar kodok

                                if (delete_kodok < 0)
                                {
                                    exit(EXIT_FAILURE);
                                }

                                if (delete_kodok == 0)
                                {
                                    execl("/usr/bin/find", "find", "/home/zygantic/modul2/animal", "-type", "f", "-name", "*frog.jpg", "-exec", "rm", "-r", "{}", "+", (char *)NULL);
                                }
                                else

```

Output:

![](./img/Output3cdarat.png)
![](./img/Output3cair.png)

## Soal 3d

### Analisa Soal

Pada soal ini, praktikan diminta untuk menghapus semua gambar burung yang ada pada directory ` /home/[USER]/modul2/darat `.

### Cara Pengerjaan

Sama seperti pada soal 3c dimana kita diminta untuk menghapus gambar kodok, hanya saja yang berbeda adalah disini kita perlu menghapus gambar yang memiliki kalimat ` bird_darat.jpg ` dan ` bird.jpg ` pada directory ` /home/zygantic/modul2/darat `.

```c++
while ((wait(&stats)) > 0)
                                        ;
                                    delete_burung_1 = fork(); // Menghapus gambar burung

                                    if (delete_burung_1 < 0)
                                    {
                                        exit(EXIT_FAILURE);
                                    }

                                    if (delete_burung_1 == 0)
                                    {
                                        execl("/usr/bin/find", "find", "/home/zygantic/modul2/darat", "-type", "f", "-name", "*bird_darat.jpg", "-exec", "rm", "-r", "{}", "+", (char *)NULL);
                                    }
                                    else
                                    {
                                        while ((wait(&stats)) > 0)
                                            ;
                                        delete_burung_2 = fork(); // Menghapus gambar burung

                                        if (delete_burung_2 < 0)
                                        {
                                            exit(EXIT_FAILURE);
                                        }

                                        if (delete_burung_2 == 0)
                                        {
                                            execl("/usr/bin/find", "find", "/home/zygantic/modul2/darat", "-type", "f", "-name", "*bird.jpg", "-exec", "rm", "-r", "{}", "+", (char *)NULL);
                                        }
```


Output:

![](./img/Output3d.png)

## Soal 3e

### Analisa Soal

Pada soal terakhir, praktikan diminta untuk membuat file ` list.txt ` pada directory ` /home/[USER]/modul2/air ` dan membuat list nama semua hewan yang ada di directory ` /home/[USER]/modul2/air ` ke dalam ` list.txt ` dengan format ` UID_[UID file permission]_Nama File.[jpg/png] ` dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.

### Kendala

Masih belum bisa mengerjakan
